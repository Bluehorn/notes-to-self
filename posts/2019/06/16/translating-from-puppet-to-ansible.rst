.. title: Translating from puppet to ansible
.. slug: translating-from-puppet-to-ansible
.. date: 2019-06-16 20:09:35 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text


Could not access my RSA key for SSH login to target site. Great, seems like
`gnome-keyring-daemon` interferes. Disabled it by editing
`/etc/xdg/autostart/gnome-keyring-ssh.desktop` adding the `Hidden=true`,
as hinted on IRC by `juliank`.

No idea if that did anything, as there was still some `ssh-agent` running.
Played with `systemctl` to get rid of the extra daemon::

    $ systemctl --user status
    $ systemctl --user status ssh-agent
    $ systemctl --user mask ssh-agent

That seemed to do the trick. To register the smart card key with AWS the
public key is needed. This is easily copy and pasted from::

    $ ssh-add -L

Next I stumbled into the issue that I want to configure the firewall of
my target hosts on a per-role basis. Seems like ansible can't do that as
the existing iptables_ module seems to just apply rules in-order.

.. _iptables: <https://docs.ansible.com/ansible/2.8/modules/iptables_module.html

Helpfully that documentation states:

    This module just deals with individual rules.If you need advanced chaining
    of rules the recommended way is to template the iptables restore file.

I found some hope in this `blog post`_ which discusses a ansible module that
keeps that of iptables, but this was never merged as it seems.

.. _blog post: https://engineering.nordeus.com/managing-iptables-with-ansible-the-easy-way/

